version: '2'
pipelines:
- mpp-import-pipelines:
    path: cs9-$image_type-image.ipp.yml
- name: image
  build: name:build
  stages:
  - type: org.osbuild.truncate
    options:
      filename: disk.img
      size:
        mpp-eval: image.size
  - type: org.osbuild.sfdisk
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
    options:
      mpp-format-json: '{image.layout}'
  - type: org.osbuild.mkfs.fat
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['efi'].start
          size:
            mpp-eval: image.layout['efi'].size
    options:
      label: ESP
      volid: 7B7795E7
  - type: org.osbuild.mkfs.ext4
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['boot'].start
          size:
            mpp-eval: image.layout['boot'].size
    options:
      uuid:
        mpp-eval: bootfs_uuid
      label: boot
  - type: org.osbuild.mkfs.ext4
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['root'].start
          size:
            mpp-eval: image.layout['root'].size
    options:
      uuid:
        mpp-eval: rootfs_uuid
      label: root
  - type: org.osbuild.copy
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
      build-tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:build
      extra-tree:
        mpp-if: "'extra_image_source_' + image_type in locals()"
        then:
          type: org.osbuild.tree
          origin: org.osbuild.pipeline
          references:
            - mpp-format-string: "name:{locals().get('extra_image_source_' + image_type)}"
    options:
      paths:
        mpp-join:
        - - from: input://tree/
            to: mount://root/
        - mpp-eval: locals().get('extra_image_copy_' + image_type, [])
    devices:
      efi:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['efi'].start
          size:
            mpp-eval: image.layout['efi'].size
      boot:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['boot'].start
          size:
            mpp-eval: image.layout['boot'].size
      root:
        type: org.osbuild.loopback
        options:
          filename: disk.img
          start:
            mpp-eval: image.layout['root'].start
          size:
            mpp-eval: image.layout['root'].size
    mounts:
    - name: root
      type: org.osbuild.ext4
      source: root
      target: /
    - name: boot
      type: org.osbuild.ext4
      source: boot
      target: /boot
    - name: efi
      type: org.osbuild.fat
      source: efi
      target: /boot/efi

- name: qcow2
  build: name:build
  stages:
  - type: org.osbuild.qemu
    inputs:
      image:
        type: org.osbuild.files
        origin: org.osbuild.pipeline
        references:
          name:image:
            file: disk.img
    options:
      filename: disk.qcow2
      format:
        type: qcow2
        compat: '1.1'

- name: container
  build: name:build
  stages:
  - type: org.osbuild.oci-archive
    inputs:
      base:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:rootfs
    options:
      filename: container.tar
      architecture: $arch
      config:
        Cmd:
        - "/usr/bin/bash"

# We need a smaller fstab for the non-partitioned case
- name: ext4-fstab
  build: name:build
  stages:
  # We copy /etc to get the right selinux context on the new file
  - type: org.osbuild.copy
    inputs:
      image-tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
    options:
      paths:
      - from: input://image-tree/etc
        to: tree:///etc
  - type: org.osbuild.fstab
    options:
      filesystems:
        - uuid:
            mpp-eval: rootfs_uuid
          vfs_type: ext4
          path: /

- name: ext4
  build: name:build
  stages:
  - type: org.osbuild.truncate
    options:
      filename: rootfs.ext4
      size:
        mpp-eval: image.size
  - type: org.osbuild.mkfs.ext4
    devices:
      device:
        type: org.osbuild.loopback
        options:
          filename: rootfs.ext4
          start: 0
          size:
            mpp-format-int: "{int(image.size) // 512}"
    options:
      uuid:
        mpp-eval: rootfs_uuid
      label: root
  - type: org.osbuild.copy
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:image-tree
      fstab:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:ext4-fstab
    options:
      paths:
        mpp-join:
        - - from: input://tree/
            to: mount://root/
          - from: input://fstab/etc/fstab
            to: mount://root/etc/fstab
        - mpp-eval: locals().get('extra_image_copy_' + image_type, [])
    devices:
      root:
        type: org.osbuild.loopback
        options:
          filename: rootfs.ext4
          start: 0
          size:
            mpp-format-int: "{int(image.size) // 512}"
    mounts:
    - name: root
      type: org.osbuild.ext4
      source: root
      target: /

- name: tar
  build: name:build
  stages:
  - type: org.osbuild.tar
    inputs:
      tree:
        type: org.osbuild.tree
        origin: org.osbuild.pipeline
        references:
        - name:rootfs
    options:
      filename: rootfs.tar
      root-node: omit
